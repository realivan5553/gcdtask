﻿using System;

namespace GcdTask {
    /// <summary>
    /// Provide methods with integers.
    /// </summary>
    public static class Program {
        static void Main(string[] args)
        {
            // method empty
        }
        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(int a, int b) {
            if (a == 0 && b == 0) {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue) {
                throw new ArgumentOutOfRangeException(nameof(a), "min value");
            }

            (a, b) = EuclideanLoop(a, b);

            return Math.Abs(a);
        }

        /// <summary>
        /// Calculates GCD of three integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="c">Third integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(int a, int b, int c) {
            if (a == 0 && b == 0 && c == 0) {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue || c == int.MinValue) {
                throw new ArgumentOutOfRangeException(nameof(a), "min value");
            }

            int[] other = new int[2] { b, c};

            int temp;
            foreach (int i in other) {
                temp = i;
                (a, temp) = EuclideanLoop(a, temp);
            }

            return Math.Abs(a);
        }

        /// <summary>
        /// Calculates the GCD of integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="other">Other integers.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(int a, int b, params int[] other) {
            if (a == 0 && b == 0 && IsAllValuesEqualsZero(other)) {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue || IsMinValueNumber(other)) {
                throw new ArgumentOutOfRangeException(nameof(a), "min value");
            }

            (a, b) = EuclideanLoop(a, b);

            int temp;
            foreach (int i in other) {
                temp = i;
                (a, temp) = EuclideanLoop(a, temp);
            }

            return Math.Abs(a);
        }

        /// <summary>
        /// Calculates GCD of two integers [-int.MaxValue;int.MaxValue] by the Stein algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int GetGcdByStein(int a, int b) {
            if (a == 0 && b == 0) {
                throw new ArgumentException("all numbers are 0 at the same time");
            }

            if (a == int.MinValue || b == int.MinValue) {
                throw new ArgumentOutOfRangeException(nameof(a), "min value");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);

            return SteinLoop(a, b);
        }

        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b) {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            a = GetGcdByEuclidean(a, b);

            watch.Stop();
            elapsedTicks = watch.ElapsedTicks;

            return a;
        }

        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue] by the Stein algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int GetGcdByStein(out long elapsedTicks, int a, int b) {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            a = GetGcdByStein(a, b);

            watch.Stop();
            elapsedTicks = watch.ElapsedTicks;

            return a;
        }

        /// <summary>
        /// Is exist value wich is equal int.MinValue in a params array
        /// </summary>
        /// <param name="other">array of other values.</param>
        /// <returns>true if exist, false otherwise.</returns>
        public static bool IsMinValueNumber(int[] other) {
            foreach (int i in other) {
                if (i == int.MinValue) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Are all values equal to zero in params array
        /// </summary>
        /// <param name="other">array of other values.</param>
        /// <returns>true if all values equals to 0, false otherwise.</returns>
        public static bool IsAllValuesEqualsZero(int[] other) {
            int count = 0;
            foreach (int i in other) {
                if (i == 0) {
                    count++;
                }
            }

            if (count == other.Length) {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>Tuple of two int values.</returns>
        public static (int, int) EuclideanLoop(int a, int b) {
            while (b != 0) {
                a %= b;
                (a, b) = (b, a);
            }

            return (a, b);
        }

        /// <summary>
        /// Calculates GCD of two integers [-int.MaxValue;int.MaxValue] by the Stein algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        public static int SteinLoop(int a, int b) {
            a = Math.Abs(a);
            b = Math.Abs(b);

            if (a == 0) {
                return b;
            }

            if (b == 0) {
                return a;
            }

            if (a == b) {
                return a;
            }

            bool val1IsEven = (a & 1u) == 0;
            bool val2IsEven = (b & 1u) == 0;
            
            if (val1IsEven && val2IsEven) {                 // Если a, b чётные, то НОД(a, b) = 2*НОД(a/2, b/2);
                return SteinLoop(a >> 1, b >> 1) << 1;
            } else if (val1IsEven && !val2IsEven) {         // Если a чётное, b нечётное, то НОД(a, b) = НОД(a/2, b);
                return SteinLoop(a >> 1, b);
            } else if (val2IsEven) {                        // Если a нечётное, b чётное, то НОД(a, b) = НОД(a, b/2);
                return SteinLoop(a, b >> 1);
            } else if (a > b) {                             // Если a, b нечётные и a > b, то НОД(a, b) = НОД((a-b)/2, b);
                return SteinLoop((a - b) >> 1, b);
            } else {                                        // Если a, b нечётные и a < b, то НОД(a, b) = НОД((b-a)/2, a);
                return SteinLoop((b - a) >> 1, a);
            }
        }

        /// <summary>
        /// Prepare data for building a histogram comparing time finding a solution for each of the algorithms.
        /// a anb b is a random values;
        /// </summary>
        /// <returns> Two arrays with length equals 9. Where each value is a elapsed time of Euclidean and Stein algorithms</returns>
        public static void EuclideanOrStein(out long[] ticksEuclidean, out long[] ticksStein) {
            int a, b;
            Random random = new Random();
            long[] tEuclidean = new long[9];
            long[] tStein = new long[9];
            for (int i = 0; i < 9; i++)
            {
                a = random.Next(int.MinValue + 1, int.MaxValue);
                b = random.Next(int.MinValue + 1, int.MaxValue);
                GetGcdByEuclidean(out long ticksE, a, b);
                GetGcdByStein(out long ticksS, a, b);
                tEuclidean[i] = ticksE;
                tStein[i] = ticksS;
            }

            ticksEuclidean = tEuclidean;
            ticksStein = tStein;
        }
    }
}